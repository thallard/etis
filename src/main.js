import { createApp } from 'vue'
import App from './App.vue'
import Bootstrap from 'bootstrap-vue-3'
import { createPinia } from 'pinia'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'

const app = createApp(App);
app.use(Bootstrap);
app.use(createPinia()).mount('#app')
